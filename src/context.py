import sqlalchemy as db

from config import config

APP_CONFIG: config.Config = None

INGEST_ENGINE: db.engine.Engine = None

RETRY_LAST_BREAK = False
SKIP_LAST_BREAK = False
TEST = False


def init_context(
    app_config: config.Config,
    ingest_engine: db.engine.Engine,
    retry_last_break: bool = False,
    skip_last_break: bool = False,
    test: bool = False,
):
    """Initialize the context."""

    # pylint: disable=global-statement
    global APP_CONFIG, INGEST_ENGINE, RETRY_LAST_BREAK, SKIP_LAST_BREAK, TEST, PRIVATE_KEY

    APP_CONFIG = app_config
    INGEST_ENGINE = ingest_engine
    RETRY_LAST_BREAK = retry_last_break
    SKIP_LAST_BREAK = skip_last_break
    TEST = test
