import time
import logging
from requests import HTTPError

from repository import dto_deletion

from .helpers import utils
from . import api_clients as api, context as ctx

logger = logging.getLogger("ingest")


def delete_documents():
    """Run the file deletion process."""

    documents = dto_deletion.get_pending_deletions()

    if len(documents) == 0:
        logger.info("No documents to delete.")
        return

    auth_token = api.login_user()

    for document in documents:

        tries = 1
        success = False

        re_login = utils.expired_token(auth_token)

        while not success and tries <= ctx.APP_CONFIG.PUSH_CHUNK_MAX_TRIES:

            logger.debug("Deleting document %s, try no: %s", document.id, tries)

            if not re_login:
                re_login = tries > 1

            if tries > 1:
                wait_seconds = utils.get_wait_seconds(tries)
                if wait_seconds:
                    logger.info("Retrying after %s seconds", wait_seconds)
                    time.sleep(wait_seconds)

            try:

                if re_login:
                    auth_token = api.login_user()
                    re_login = False

                api.delete_document(document_id=document.doc_id, token=auth_token)

                success = True
                logger.debug("Document %s deleted.", document.doc_id)

            except HTTPError as error:

                if error.response.status_code == 401:
                    re_login = True
                    tries += 1

                    continue

                if (
                    error.response.status_code in (400, 403)
                    or tries >= ctx.APP_CONFIG.PUSH_CHUNK_MAX_TRIES
                ):
                    raise

                logger.error(
                    "Error: %s, tries: %s",
                    error.response.text,
                    tries,
                )
                tries += 1

        dto_deletion.update_deletion_status(doc_id=document.id, status="completed")
