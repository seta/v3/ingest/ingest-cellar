import json
import requests

from .helpers.utils import generate_signature
from . import context as ctx
from . import ingest_exception as exc


def login_user():
    """Gets authorization token for configured user."""

    message, signature = generate_signature(ctx.APP_CONFIG.PRIVATE_KEY)

    payload = {
        "username": ctx.APP_CONFIG.AUTH_USERNAME,
        "provider": ctx.APP_CONFIG.AUTH_PROVIDER.lower(),
        "rsa_original_message": message,
        "rsa_message_signature": signature,
    }

    data = json.dumps(payload)
    response = requests.post(
        ctx.APP_CONFIG.AUTH_URL,
        data=data,
        headers={"Content-Type": "application/json"},
        timeout=30,
    )

    if response.status_code != 200:
        # pylint: disable-next=broad-exception-raised
        raise exc.FailedAuthentication(f"Failed to login user: {response.text}")

    return response.json()["access_token"]


def push_data(data: str, token: str):
    """Pushes data to the configured endpoint."""

    response = requests.post(
        ctx.APP_CONFIG.SEARCH_API_URL + ctx.APP_CONFIG.INGEST_ENDPOINT,
        data=data,
        headers={
            "Content-Type": "application/json",
            "Authorization": f"Bearer {token}",
        },
        timeout=30,
    )

    response.raise_for_status()


def delete_document(document_id: str, token: str):
    """Deletes document from the configured endpoint."""

    response = requests.delete(
        ctx.APP_CONFIG.SEARCH_API_URL + ctx.APP_CONFIG.DELETE_ENDPOINT,
        data=json.dumps({"document_id": document_id}),
        headers={
            "Content-Type": "application/json",
            "Authorization": f"Bearer {token}",
        },
        timeout=30,
    )

    response.raise_for_status()
