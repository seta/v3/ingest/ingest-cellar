import time
import logging
from requests import HTTPError

from .helpers import utils
from . import api_clients as api, ingest_exception as exc, context as ctx

logger = logging.getLogger("ingest")


def ingest_file(file_path: str, start_line: int = 1) -> int:
    """Run the file ingest process."""

    chunks_no = 0

    auth_token = api.login_user()

    with open(file_path, "r", encoding="utf-8") as file:
        for line_no, line in enumerate(file):

            if line_no < start_line - 1:
                continue

            re_login = utils.expired_token(auth_token) or chunks_no % 1000 == 0
            tries = 1

            success_push = False

            while not success_push and tries <= ctx.APP_CONFIG.PUSH_CHUNK_MAX_TRIES:

                logger.debug(
                    "Pushing line %s from file %s, try no: %s",
                    line_no + 1,
                    file_path,
                    tries,
                )

                if not re_login:
                    re_login = tries > 1

                if tries > 1:
                    wait_seconds = utils.get_wait_seconds(tries)
                    if wait_seconds:
                        logger.info("Retrying after %s seconds", wait_seconds)
                        time.sleep(wait_seconds)

                try:

                    if re_login:
                        auth_token = api.login_user()
                        re_login = False

                    api.push_data(line, auth_token)

                    success_push = True

                except exc.FailedAuthentication as error:
                    raise exc.IngestException(
                        file_path=file_path,
                        line=line_no + 1,
                        error=str(error),
                        tries=tries,
                    )

                except HTTPError as error:

                    if error.response.status_code == 401:
                        re_login = True
                        tries += 1

                        continue

                    if (
                        error.response.status_code in (400, 403)
                        or tries >= ctx.APP_CONFIG.PUSH_CHUNK_MAX_TRIES
                    ):
                        raise exc.IngestException(
                            file_path=file_path,
                            line=line_no + 1,
                            error=str(error),
                            tries=tries,
                        )

                    logger.error(
                        "Error pushing data to the endpoint: %s, tries: %s",
                        error.response.text,
                        tries,
                    )
                    tries += 1

            logger.debug(
                "Line %s pushed successfully from file %s", line_no + 1, file_path
            )

            chunks_no += 1

    return chunks_no
