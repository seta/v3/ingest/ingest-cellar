class IngestException(Exception):
    file_path: str
    line: int
    error: str
    tries: int

    def __init__(self, file_path: str, line: int, error: str, tries: int):
        self.file_path = file_path
        self.line = line
        self.error = error
        self.tries = tries


class FailedAuthentication(Exception):
    def __init__(self, message: str):
        self.message = message
