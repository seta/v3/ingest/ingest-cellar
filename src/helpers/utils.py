import random
import string
import datetime as dt
import jwt

from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15


def generate_signature(private_key: str) -> tuple[str, str]:
    """
    Generate a random message and its signature bases on a private key

    :param private_key:
        The encoded private key
    """

    key = RSA.import_key(private_key)
    # create a message
    random_string = "".join((random.choice(string.ascii_lowercase) for x in range(20)))
    # Signing
    digest = SHA256.new(random_string.encode())
    signature = pkcs1_15.new(key).sign(digest)

    return random_string, signature.hex()


def expired_token(token: str):
    """
    Check if a token is expired

    :param token:
        The token to check
    """

    token_expires = dt.timedelta(minutes=10)

    decoded_token = jwt.decode(token, options={"verify_signature": False})

    exp_timestamp = decoded_token.get("exp", None)

    if not exp_timestamp:
        return False

    now = dt.datetime.now(dt.timezone.utc)
    expire_minutes = (token_expires.total_seconds() / 60) // 2
    delta = dt.timedelta(minutes=expire_minutes)
    target_timestamp = dt.datetime.timestamp(now + delta)
    return target_timestamp > exp_timestamp


def get_wait_seconds(tries: int):
    """
    Get the number of seconds to wait based on the number of tries

    :param tries:
        The number of tries
    """

    match tries:
        case 2:
            return 10
        case 3:
            return 60
        case 4:
            return 60 * 10
        case 5:
            return 60 * 15
        case 6:
            return 60 * 30
        case _:
            return 0
