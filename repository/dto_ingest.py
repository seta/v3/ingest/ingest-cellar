"""Data Transfer Object for Ingestion."""

import datetime as dt
from sqlalchemy.orm import Session

from src import context as ctx
from .models import ingest_models as models


def get_pending_ingestion() -> list[models.IngestionFile]:
    """Get all pending ingestion."""

    with Session(ctx.INGEST_ENGINE) as session:
        return (
            session.query(models.IngestionFile)
            .filter(models.IngestionFile.status == "pending")
            .order_by(models.IngestionFile.created_at)
            .all()
        )


def update_ingestion_status(ingestion_id: int, status: str):
    """Update the status of an ingestion."""

    with Session(ctx.INGEST_ENGINE) as session:
        ingestion = session.query(models.IngestionFile).get(ingestion_id)
        ingestion.status = status
        ingestion.updated_at = dt.datetime.now(dt.timezone.utc)
        session.commit()


def complete_ingestion(ingestion_id: int, new_path: str):
    """Mark an ingestion as completed."""

    with Session(ctx.INGEST_ENGINE) as session:
        ingestion = session.query(models.IngestionFile).get(ingestion_id)
        ingestion.status = "completed"
        ingestion.file_path = new_path
        ingestion.updated_at = dt.datetime.now(dt.timezone.utc)
        session.commit()
