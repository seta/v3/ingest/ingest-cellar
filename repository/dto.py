import sqlalchemy as db
from .models import ingest_models


def init_ingest_db(sqlite_db: str, echo: bool = False) -> db.engine.Engine:
    """Gets the engine."""

    engine = db.create_engine(sqlite_db, echo=echo)

    ingest_models.Base.metadata.create_all(engine)

    return engine
