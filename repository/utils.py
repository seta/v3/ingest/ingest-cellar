def ingest_db(dir_path: str, test_mode: bool = False) -> str:
    """Gets sqlite database connection string."""

    if test_mode:
        return f"sqlite:///{dir_path}/test_ingestion.db"

    return f"sqlite:///{dir_path}/ingestion.db"
