"""Data Transfer Object for IngestionBreak."""

import datetime as dt
from sqlalchemy.orm import Session

from src import context as ctx
from .models import ingest_models as models


def get_ingestion_break() -> models.IngestionBreak:
    """Get the last ingestion break."""

    with Session(ctx.INGEST_ENGINE) as session:
        return session.query(models.IngestionBreak).first()


def set_ingestion_break(ingestion_id: int, line_no: int, tries: int, error: str):
    """Create a new ingestion break."""

    with Session(ctx.INGEST_ENGINE) as session:

        existing_break = session.query(models.IngestionBreak).first()
        if existing_break:
            existing_break.ingestion_id = ingestion_id
            existing_break.line_no = line_no
            existing_break.break_at = dt.datetime.now(dt.timezone.utc)
            existing_break.tries = tries
            existing_break.last_error = error
        else:
            ingestion_break = models.IngestionBreak(
                ingestion_id=ingestion_id,
                line_no=line_no,
                break_at=dt.datetime.now(dt.timezone.utc),
                tries=tries,
                last_error=error,
            )
            session.add(ingestion_break)
        session.commit()


def clear_ingestion_break():
    """Clear the last ingestion break."""

    with Session(ctx.INGEST_ENGINE) as session:
        existing_break = session.query(models.IngestionBreak).first()
        if existing_break:
            session.delete(existing_break)
            session.commit()
