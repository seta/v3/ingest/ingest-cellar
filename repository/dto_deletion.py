"""Data Transfer Object for Deletion."""

import datetime as dt
from sqlalchemy.orm import Session

from src import context as ctx
from .models import ingest_models as models


def get_pending_deletions() -> list[models.DeletionDocument]:
    """Get all pending deletions."""

    with Session(ctx.INGEST_ENGINE) as session:
        return (
            session.query(models.DeletionDocument)
            .filter(models.DeletionDocument.status == "pending")
            .order_by(models.DeletionDocument.created_at)
            .all()
        )


def update_deletion_status(doc_id: int, status: str):
    """Update the status of an ingestion."""

    with Session(ctx.INGEST_ENGINE) as session:
        ingestion = session.query(models.DeletionDocument).get(doc_id)
        ingestion.status = status
        ingestion.updated_at = dt.datetime.now(dt.timezone.utc)
        session.commit()
