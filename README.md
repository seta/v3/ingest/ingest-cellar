# Ingest Service

## Description
Push chunks to Search engine using SeTA Search API.

## Environment

The following environment variables are required at runtime:
* LOG_LEVEL, defaults to INFO
* AUTH_URL - SETA authentication web service
* SEARCH_API_URL - search web service

For development, you can create a `.env` file with these variables.

## Usage

Save the private key to `./private/id.rsa` file - check `PRIVATE_KEY_PATH` value in `config/config/py` file.
The registered user should have 'Data Owner' permission for ingested data sources.

`python3 ingest.py`

Options:
    --help Show this message and exit.
    --retry-last-break Retry sending last failed line, yes/no - default no.
    --skip-last-break Skip sending last failed line, yes/no - default no.
    --test Run in test mode, yes/no - default no.

### Build Image for Container Registry 
Build production image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/data/ingest-service:latest .
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/data/ingest-service:latest
```