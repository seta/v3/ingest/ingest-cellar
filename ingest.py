import argparse
import logging
import pathlib

from dotenv import load_dotenv

from config import config, log_init
from repository import dto, dto_ingest, dto_break, utils

from src import context as ctx
from src import file_ingest, file_deletion
from src.helpers import stopwatch as sw
from src.ingest_exception import IngestException

load_dotenv()  # take environment variables from .env, if exists

log_init.init_logging()
logger = logging.getLogger("ingest")

parser = argparse.ArgumentParser(
    description="Push data to SeTA Search from output files."
)
parser.add_argument(
    "--retry-last-break",
    help="Retry sending last failed line, yes/no - default no.",
    default="no",
)
parser.add_argument(
    "--skip-last-break",
    help="Skip sending last failed line, yes/no - default no.",
    default="no",
)
parser.add_argument(
    "--test", help="Run in test mode, yes/no - default no.", default="no"
)


def init():
    """Initialize the context."""
    args = parser.parse_args()

    retry_last_break = (
        args.retry_last_break is not None and args.retry_last_break.lower() == "yes"
    )
    skip_last_break = (
        args.skip_last_break is not None and args.skip_last_break.lower() == "yes"
    )
    test_mode = args.test is not None and args.test.lower() == "yes"

    configuration = config.Config(test_mode)

    pathlib.Path(configuration.SQLITE_DIR).mkdir(parents=True, exist_ok=True)
    pathlib.Path(configuration.COMPLETED_DIR).mkdir(parents=True, exist_ok=True)

    ingest_db = utils.ingest_db(configuration.SQLITE_DIR, test_mode=test_mode)
    ingest_engine = dto.init_ingest_db(ingest_db, echo=False)

    ctx.init_context(
        app_config=configuration,
        ingest_engine=ingest_engine,
        retry_last_break=retry_last_break,
        skip_last_break=skip_last_break,
        test=test_mode,
    )


def main():
    """Main function."""

    init()

    file_deletion.delete_documents()

    _ingest()


def _ingest_file(output_file, last_break):
    if last_break is not None and last_break.ingestion_id == output_file.id:
        start_line = last_break.line_no
        if ctx.SKIP_LAST_BREAK:
            start_line += 1
    else:
        start_line = 1

    stopwatch = sw.Stopwatch()
    stopwatch.start()

    chunks_no = 0

    try:
        chunks_no = file_ingest.ingest_file(output_file.file_path, start_line)
    except IngestException as ie:
        dto_break.set_ingestion_break(
            ingestion_id=output_file.id,
            line_no=ie.line,
            tries=ie.tries,
            error=ie.error,
        )
        raise

    stopwatch.stop()

    logger.info(
        "Ingestion of %s chunks from %s took %s seconds.",
        chunks_no,
        output_file.file_path,
        stopwatch.duration,
    )

    if last_break is not None and last_break.ingestion_id == output_file.id:
        dto_break.clear_ingestion_break()

    try:
        # move the file to the completed directory
        file_name = pathlib.Path(output_file.file_path).name
        completed_path = pathlib.Path(output_file.file_path).rename(
            pathlib.Path(ctx.APP_CONFIG.COMPLETED_DIR, file_name)
        )

        dto_ingest.complete_ingestion(
            ingestion_id=output_file.id, new_path=completed_path.as_posix()
        )
    except Exception as e:
        logger.error(
            "Error moving file %s to the completed directory: %s",
            output_file.file_path,
            e,
        )

        dto_ingest.update_ingestion_status(
            ingestion_id=output_file.id, status="completed"
        )


def _ingest():
    last_break = dto_break.get_ingestion_break()

    output_files = dto_ingest.get_pending_ingestion()
    if not output_files:
        dto_break.clear_ingestion_break()

        logger.info("No pending ingestion.")
        return

    # check if theres a line break only for the first file
    if (
        last_break is not None
        and not (ctx.RETRY_LAST_BREAK or ctx.SKIP_LAST_BREAK)
        and last_break.tries >= ctx.APP_CONFIG.PUSH_CHUNK_MAX_TRIES
    ):
        # pylint: disable=broad-exception-raised
        raise Exception(
            f"Last break has been tried {last_break.tries} times. Investigate the problem or use one of the options: --retry-last-break=yes --skip-last-break=yes"
        )

    for output_file in output_files:

        if not pathlib.Path(output_file.file_path).exists():
            dto_ingest.update_ingestion_status(
                ingestion_id=output_file.id, status="not-found"
            )
            logger.error("File %s not found.", output_file.file_path)
            continue

        _ingest_file(output_file, last_break)


if __name__ == "__main__":
    try:
        main()
    except Exception as exc:
        logger.exception("error: %s", exc)

        raise SystemExit(1) from exc
    finally:
        if ctx.INGEST_ENGINE is not None:
            ctx.INGEST_ENGINE.dispose()
