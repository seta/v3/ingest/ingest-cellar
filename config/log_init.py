import os
import pathlib
import logging
import logging.config


def init_logging() -> str:
    """Initialize logging."""

    logs_file_path = os.environ.get("LOGS_FILE_PATH", None)

    if os.environ.get("LOGS_FILE_PATH", None) is None:
        logs_file_path = "/media/ingest/logs/ingest.log"
        os.environ["LOGS_FILE_PATH"] = logs_file_path

    file_path = pathlib.Path(logs_file_path)
    file_path.parent.mkdir(parents=True, exist_ok=True)

    logging.config.fileConfig("./config/logging.conf", disable_existing_loggers=False)

    log_level = os.getenv("LOG_LEVEL", default="INFO")
    logging.getLogger().setLevel(log_level)
    logging.getLogger("ingest").setLevel(log_level)
