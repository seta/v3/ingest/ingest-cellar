import os
import configparser


class Config:
    """Application configuration"""

    SQLITE_DIR = ""
    COMPLETED_DIR = ""

    AUTH_USERNAME = ""
    AUTH_PROVIDER = ""
    AUTH_URL = ""
    SEARCH_API_URL = ""
    INGEST_ENDPOINT = ""

    PRIVATE_KEY = None
    PRIVATE_KEY_PATH = "./private/id.rsa"

    PUSH_CHUNK_MAX_TRIES = 6

    def __init__(self, test_mode: bool = False):
        config_parser = configparser.ConfigParser(
            os.environ, interpolation=configparser.ExtendedInterpolation()
        )
        config_parser.read("./config/app.conf")

        sections = config_parser.sections()
        if len(sections) == 0:
            # pylint: disable-next=broad-exception-raised
            raise Exception("No configuration section found in the 'app.conf' file")

        section_name = "APP" if not test_mode else "APP_TEST"
        if section_name not in sections:
            # pylint: disable-next=broad-exception-raised
            raise Exception("APP must be one of " + str(sections))
        Config._init_app(config_section=config_parser[section_name])

        section_name = "SETA"
        if section_name not in sections:
            # pylint: disable-next=broad-exception-raised
            raise Exception("SETA must be one of " + str(sections))
        Config._init_seta(config_section=config_parser[section_name])

        with open(Config.PRIVATE_KEY_PATH, "r", encoding="utf-8") as f:
            Config.PRIVATE_KEY = f.read()

    @staticmethod
    def _init_app(config_section: configparser.SectionProxy):
        """Initialize  application config"""

        Config.SQLITE_DIR = config_section["SQLITE_DIR"]
        Config.COMPLETED_DIR = config_section.get("COMPLETED_DIR")

    @staticmethod
    def _init_seta(config_section: configparser.SectionProxy):
        """Initialize SETA config"""

        Config.AUTH_USERNAME = config_section.get("AUTH_USERNAME")
        Config.AUTH_PROVIDER = config_section.get("AUTH_PROVIDER")

        Config.INGEST_ENDPOINT = config_section.get("INGEST_ENDPOINT")
        Config.DELETE_ENDPOINT = config_section.get("DELETE_ENDPOINT")

        auth_url = config_section.get("AUTH_URL")
        Config.AUTH_URL = os.getenv("AUTH_URL", auth_url)

        search_api_url = config_section.get("SEARCH_API_URL")
        Config.SEARCH_API_URL = os.getenv("SEARCH_API_URL", search_api_url)
